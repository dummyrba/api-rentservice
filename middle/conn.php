<?php
//error_reporting(E_ALL);
//error_reporting(0);

if (!defined('_VALID_ACCESS')) die('Permintaan file ditolak!');

date_default_timezone_set('Asia/Jakarta');

// $host = "localhost";
// $db = "workteam_rentalservice";
// $user = "workteam_root";
// $pass = "r@2r10XGEdDz";

$host = "localhost";
$db = "rentalservice";
$user = "root";
$pass = "";

$gFirebaseAPIKey = "AAAAtvQa3fo:APA91bFxJ3DGQuilXvq86GiKR7u7Z0LzUM989C0t-o553Cm1z15xqzotVMNlv22Ct7z8uckVxNNQ29idWOmByViKMZU8-D0m2l0_f89A0bxWB40aTWGJkRGOxn8r8_JRltFPoPCdQLQ4";
// $gFirebaseAPIKey = "AIzaSyCEmvHdpHVPQ3TDefAfRrxLNiFoEq07JRo";

$gClientProjectId = "rentalservice";
$gBaseUrl = "http://" . $host . "/rentalservice/";
$gDownloadUrl = $gBaseUrl . "downloads/";
$gExportUrl = $gBaseUrl . "exports/index.php?";
$gUploadUrl = $gBaseUrl . "uploads/";

$charset = 'utf8';

$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
$options = [
  PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
  PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
  PDO::ATTR_EMULATE_PREPARES   => false,
];
try {
  $gPDO = new PDO($dsn, $user, $pass, $options);
} catch (\PDOException $e) {
  throw new \PDOException($e->getMessage(), (int)$e->getCode());
}
