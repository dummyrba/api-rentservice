<?php
define('_VALID_ACCESS', TRUE);
include "../middle/conn.php";
include "../middle/functions.php";
require "../vendor/autoload.php";

use Fcm\FcmClient;
use Fcm\Push\Notification;

header("Content-type: application/json");

$errMsg = "";

$result = "";
//
if (isset($_POST["act"]))  $act = trim($_POST["act"]);
if (isset($_GET["act"]))   $act = trim($_GET["act"]);

if (isset($act) && trim($act) != "") {

    if ($act == "register_konsumen") {
        if ($_SERVER["REQUEST_METHOD"] != "POST") {
            echo composeReply("ERROR", "[Routing ERROR] Terjadi kesalahan internal.");
            exit;
        }

        if (!isset($_POST["id"]) || trim($_POST["id"]) == "") {
            echo composeReply("ERROR", "Harap isikan nomor ponsel Anda");
            exit;
        }
        $id = formatPonsel(trim($_POST["id"]), "0");

        if (!isset($_POST["name"]) || trim($_POST["name"]) == "") {
            echo composeReply("ERROR", "Harap isikan nama Anda");
            exit;
        }
        $name = trim($_POST["name"]);

        if (!isset($_POST["alamat"]) || trim($_POST["alamat"]) == "") {
            echo composeReply("ERROR", "Harap isikan alamat Anda");
            exit;
        }
        $alamat = trim($_POST["alamat"]);


        $ktp = trim($_POST["ktp"]);
        if (!isset($_POST["ktp"]) || trim($_POST["ktp"]) == "") {
            $ktp = "-";
        }


        if (isset($_POST["passwordEnc"]) && trim($_POST["passwordEnc"]) != "") $passwordEnc = trim($_POST["passwordEnc"]);
        if (isset($passwordEnc)) {
            require "../middle/MCrypt.php";
            $mcrypt = new MCrypt();
            #Encrypt
            //$encrypted = $mcrypt->encrypt("Text to encrypt");

            #Decrypt
            $decrypted = $mcrypt->decrypt($passwordEnc);
            //overwrite
            $password = $decrypted;
        } else {
            if (!isset($_POST["password"]) || trim($_POST["password"]) == "") {
                echo composeReply("ERROR", "Password harus diisi");
                exit;
            }
            $password = trim($_POST["password"]);
        }


        if (!isset($_POST["email"]) || trim($_POST["email"]) == "") {
            echo composeReply("ERROR", "Harap isikan email Anda");
            exit;
        }
        $email = trim($_POST["email"]);

        $role = trim($_POST["role"]);
        if (!isset($_POST["role"]) || trim($_POST["role"]) == "") {
            $role = "customer";
        }


        //cek apakah id available
        //pake PDO prepare krn query ini terima input dari user -> menghindari sql injection
        $stmt = $gPDO->prepare("SELECT * FROM _users WHERE U_ID = ?");
        $stmt->execute(array($id));
        $userData = $stmt->fetch(PDO::FETCH_OBJ);
        if ($userData) {
            echo composeReply("ERROR", "Maaf, nomor ponsel sudah pernah digunakan. Silahkan gunakan nomor ponsel lain.");
            exit;
        }

        //CEK KTP
        // $stmt = $gPDO->prepare("SELECT * FROM _users WHERE U_AUTHORITY_ID_1 = ?");
        // $stmt->execute(array($ktp));
        // $userData = $stmt->fetch(PDO::FETCH_OBJ);
        // if ($userData) {
        //     echo composeReply("ERROR", "Maaf, nomor KTP sudah pernah digunakan. Silahkan gunakan nomor KTP lain.");
        //     exit;
        // }

        $stmt = $gPDO->prepare("SELECT * FROM _users WHERE U_EMAIL = ?");
        $stmt->execute(array($email));
        $userData = $stmt->fetch(PDO::FETCH_OBJ);
        if ($userData) {
            echo composeReply("ERROR", "Maaf, alamat email sudah pernah digunakan. Silahkan gunakan alamat email lain.");
            exit;
        }

        $gPDO->prepare("INSERT INTO _users (
            U_ID,
            U_PASSWORD,
            U_PASSWORD_HASH,
            U_NAME,
            U_ADDRESS,
            U_AUTHORITY_ID_1,
            U_REG_DATE,
            U_PHONE,
            U_GROUP_ROLE,
            U_EMAIL) VALUES (?,?,?,?,?,?,?,?,?,?)")->execute(array(
            $id,
            $password,
            md5($password),
            strtoupper($name),
            $alamat,
            $ktp,
            date("Y-m-d"),
            $id,
            $role,
            $email
        ));

        //cek apakah data user berhasil diinput
        $stmt = $gPDO->prepare("SELECT * FROM _users WHERE U_ID = ?");
        $stmt->execute(array($id));
        $userData = $stmt->fetch(PDO::FETCH_OBJ);
        if ($userData) {
            if (isset($loginToken)) {
                echo composeReply("SUCCESS", "Proses registrasi berhasil.\Pengguna bisa login menggunakan nomor ponsel/email dan password sama dengan nomor ponsel.");
            } else {
                echo composeReply("SUCCESS", "Proses registrasi berhasil.\nAnda bisa login menggunakan nomor ponsel/email dan password yang telah dimasukkan.");
            }
            exit;
        } else {
            echo composeReply("ERROR", "Proses registrasi gagal. Harap hubungi CS (" . getSetting("CUST_SERVICE_PHONE") . ")");
            exit;
        }
    }

    if ($act == "login") {
        if ($_SERVER["REQUEST_METHOD"] != "POST") {
            echo composeReply("ERROR", "[Routing ERROR] Terjadi kesalahan internal.");
            exit;
        }

        if (!isset($_POST["id"]) || trim($_POST["id"]) == "") {
            echo composeReply("ERROR", "Harap isikan nomor ponsel/email Anda");
            exit;
        }
        $id = trim($_POST["id"]);

        if (!isset($_POST["U_FCM_TOKEN"]) || trim($_POST["U_FCM_TOKEN"]) == "") {
            echo composeReply("ERROR", "Harap isikan FCM Token");
            exit;
        }
        $fcmToken = trim($_POST["U_FCM_TOKEN"]);

        if (isset($_POST["passwordEnc"]) && trim($_POST["passwordEnc"]) != "") $passwordEnc = trim($_POST["passwordEnc"]);
        if (isset($passwordEnc)) {
            require "../middle/MCrypt.php";
            $mcrypt = new MCrypt();
            #Encrypt
            //$encrypted = $mcrypt->encrypt("Text to encrypt");

            #Decrypt
            $decrypted = $mcrypt->decrypt($passwordEnc);
            //overwrite
            $password = $decrypted;
        } else {
            if (!isset($_POST["password"]) || trim($_POST["password"]) == "") {
                echo composeReply("ERROR", "Password harus diisi");
                exit;
            }
            $password = trim($_POST["password"]);
        }

        //pake PDO prepare krn query ini terima input dari user -> menghindari sql injection
        $stmt = $gPDO->prepare("SELECT * FROM _users WHERE (U_ID = ? OR U_EMAIL = ? OR U_NAME = ?) AND U_PASSWORD_HASH = ? AND U_STATUS = 'USER_ACTIVE'");
        $stmt->execute([$id, $id, $id, md5($password)]);
        $userData = $stmt->fetch(PDO::FETCH_OBJ);
        if (!$userData) {
            echo composeReply("ERROR", "User tidak dikenal atau account belum aktif");
            exit;
        }

        $loginToken = substr(md5($id . date("YmdHis")), 0, 20);
        $gPDO->prepare("UPDATE _users SET U_LOGIN_TOKEN = ?, U_FCM_TOKEN = ? WHERE U_ID = ?")->execute([$loginToken, $fcmToken, $userData->{"U_ID"}]);

        echo composeReply("SUCCESS", "Selamat datang " . strtoupper($userData->{"U_NAME"}), array(
            "U_ID" => $userData->{"U_ID"},
            "U_NAME" => $userData->{"U_NAME"},
            "U_ADDRESS" => $userData->{"U_ADDRESS"},
            "U_AUTHORITY_ID_1" => $userData->{"U_AUTHORITY_ID_1"},
            "U_EMAIL" => $userData->{"U_EMAIL"},
            "U_LOGIN_TOKEN" => $loginToken,
            "U_PHONE" => $userData->{"U_PHONE"},
            "U_ACCT_VERIFY_IMG" => $userData->{"U_ACCT_VERIFY_IMG"},
            "U_GROUP_ROLE" => $userData->{"U_GROUP_ROLE"}
        ));
        exit;
    }

    if ($act == "get_user_byrole") {
        if ($_SERVER["REQUEST_METHOD"] != "POST") {
            echo composeReply("ERROR", "[Routing ERROR] Internal error.");
            exit;
        }

        if (!isset($_POST["loginToken"]) || trim($_POST["loginToken"]) == "") {
            echo composeReply("ERROR", "Silahkan login dahulu untuk mengakses fitur ini");
            exit;
        }
        $loginToken = trim($_POST["loginToken"]);

        $stmt = $gPDO->prepare("SELECT * FROM _users WHERE U_LOGIN_TOKEN = ?");
        $stmt->execute([$loginToken]);
        $loginData = $stmt->fetch(PDO::FETCH_OBJ);
        if (!$loginData) {
            echo composeReply("ERROR", "User tidak dikenal", array("API_ACTION" => "LOGOUT"));
            exit;
        }

        if (!isset($_POST["role"]) || trim($_POST["role"]) == "") {
            //Get User All
            $stmt = $gPDO->query("SELECT * FROM _users");
            $userData = $stmt->fetchAll(PDO::FETCH_OBJ);
            if (!$userData) {
                echo composeReply("SUCCESS", "User Tidak Dimukan");
                exit;
            } else {
                echo composeReply("SUCCESS", "All User", $userData);
                exit;
            }
        } else if (!isset($_POST["role"]) || trim($_POST["role"]) == "admin") {
            $stmt = $gPDO->query("SELECT * FROM  _users WHERE U_GROUP_ROLE = 'admin'");
            $userData = $stmt->fetchAll(PDO::FETCH_OBJ);
            if (!$userData) {
                echo composeReply("SUCCESS", "User Admin Tidak Ada");
                exit;
            } else {
                echo composeReply("SUCCESS", "Admin User", $userData);
                exit;
            }
        } else if (!isset($_POST["role"]) || trim($_POST["role"]) == "customer") {
            $stmt = $gPDO->query("SELECT * FROM  _users WHERE U_GROUP_ROLE = 'customer'");
            $userData = $stmt->fetchAll(PDO::FETCH_OBJ);
            if (!$userData) {
                echo composeReply("SUCCESS", "User Customer Tidak Ada");
                exit;
            } else {
                echo composeReply("SUCCESS", "Customer User", $userData);
                exit;
            }
        } else if (!isset($_POST["role"]) || trim($_POST["role"]) == "-") {
            $stmt = $gPDO->query("SELECT * FROM  _users WHERE U_GROUP_ROLE = 'admin' OR U_GROUP_ROLE = 'superadmin'");
            $userData = $stmt->fetchAll(PDO::FETCH_OBJ);
            if (!$userData) {
                echo composeReply("SUCCESS", "User Customer Tidak Ada");
                exit;
            } else {
                echo composeReply("SUCCESS", "ADMIN / SUPERADMIN User", $userData);
                exit;
            }
        }
    }

    if ($act == "create_ktp") {
        if ($_SERVER["REQUEST_METHOD"] != "POST") {
            echo composeReply("ERROR", "[Routing ERROR] Internal error.");
            exit;
        }

        if (!isset($_POST["loginToken"]) || trim($_POST["loginToken"]) == "") {
            echo composeReply("ERROR", "Silahkan login dahulu untuk mengakses fitur ini");
            exit;
        }
        $loginToken = trim($_POST["loginToken"]);

        $stmt = $gPDO->prepare("SELECT * FROM _users WHERE U_LOGIN_TOKEN = ?");
        $stmt->execute([$loginToken]);
        $loginData = $stmt->fetch(PDO::FETCH_OBJ);
        if (!$loginData) {
            echo composeReply("ERROR", "User tidak dikenal", array("API_ACTION" => "LOGOUT"));
            exit;
        }

        if (isset($_FILES["uploadFile"])) {
            $fileName = $_FILES['uploadFile']['name'];
            $fileSize = $_FILES['uploadFile']['size'];
            $fileTmp = $_FILES['uploadFile']['tmp_name'];
            $fileType = $_FILES['uploadFile']['type'];
            $fileError = $_FILES['uploadFile']['error'];

            $a = explode(".", $_FILES["uploadFile"]["name"]);
            $fileExt = strtolower(end($a));

            if (isset($fileError) && $fileError > 0) {
                $FILE_UPLOAD_ERROR_INFO = array(
                    0 => 'There is no error, the file uploaded with success',
                    1 => 'Ukuran file terlalu besar'/*'The uploaded file exceeds the upload_max_filesize directive in php.ini'*/,
                    2 => 'Ukuran file terlalu besar'/*'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form'*/,
                    3 => 'Terjadi gangguan jaringan sehingga data terpotong'/*'The uploaded file was only partially uploaded'*/,
                    4 => 'Tidak ada file yang diunggah',
                    6 => 'Temporary folder tidak tersedia',
                    7 => 'Gagal menyimpan data',
                    8 => /*'A PHP extension stopped the file upload.'*/ 'Terjadi kesalahan internal di server',
                );

                echo composeReply("ERROR", $FILE_UPLOAD_ERROR_INFO[$fileError]);
                exit;
            }

            $arrFileExt = array("jpg", "jpeg", "png");
            if (isset($fileName) && trim($fileName) != "") {
                if (in_array($fileExt, $arrFileExt) === false) {
                    echo composeReply("ERROR", "Harap pilih format image yang sesuai");
                    exit;
                }

                $uploadFile = substr(md5(date("YmdHis")), 0, 5) . "." . $fileExt;
                if (move_uploaded_file($fileTmp, "../uploads/" . $uploadFile)) {
                    $gPDO->prepare("UPDATE _users SET U_AUTHORITY_ID_1 = ? WHERE U_LOGIN_TOKEN = ?")->execute([$uploadFile, $loginToken]);
                    echo composeReply("SUCCESS", "Foto KTP Berhasil di Upload", array(
                        "U_AUTHORITY_ID_1" => $uploadFile
                    ));
                    exit;
                } else {
                    echo composeReply("ERROR", "Gagal menyimpan Foto KTP");
                    exit;
                }
            } else {
                echo composeReply("ERROR", "Gagal menyimpan Foto KTP");
                exit;
            }
        } else {
            echo composeReply("ERROR", "Harap upload file");
            exit;
        }
    }

    if ($act == "delete_customer") {
        if ($_SERVER["REQUEST_METHOD"] != "POST") {
            echo composeReply("ERROR", "[Routing ERROR] Internal error.");
            exit;
        }

        if (isset($_POST["loginToken"]) && trim($_POST["loginToken"]) != "")     $loginToken = trim($_POST["loginToken"]);
        if (!isset($loginToken)) {
            echo composeReply("ERROR", "Akses tidak dikenal", array("API_ACTION" => "LOGOUT"));
            exit;
        }

        $stmt = $gPDO->prepare("SELECT * FROM _users WHERE U_LOGIN_TOKEN = ?");
        $stmt->execute([$loginToken]);
        $userData = $stmt->fetch(PDO::FETCH_OBJ);
        if (!$userData) {
            echo composeReply("ERROR", "User tidak dikenal", array("API_ACTION" => "LOGOUT"));
            exit;
        }

        if (isset($_POST["U_ID"]) && trim($_POST["U_ID"]) != "") $U_ID = trim(strtoupper($_POST["U_ID"]));
        if (!isset($U_ID)) {
            echo composeReply("ERROR", "Parameter tidak lengkap");
            exit;
        }

        $stmt = $gPDO->prepare("SELECT * FROM _users WHERE U_ID = ?");
        $stmt->execute([$U_ID]);
        $orderData = $stmt->fetch(PDO::FETCH_OBJ);
        if (!$orderData) {
            echo composeReply("ERROR", "Customer tidak dikenal");
            exit;
        }

        $gPDO->prepare("DELETE FROM _users WHERE U_ID = ?")->execute([$U_ID]);

        echo composeReply("SUCCESS", "Customer berhasil di hapus");
        exit;
    }
} else {
    echo composeReply("ERROR", "[Routing ERROR] Terjadi kesalahan internal.");
    exit;
}
