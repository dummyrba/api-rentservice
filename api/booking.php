<?php
define('_VALID_ACCESS', TRUE);
include "../middle/conn.php";
include "../middle/functions.php";

header("Content-type: application/json");

$errMsg = "";

$result = "";
//
if (isset($_POST["act"]))  $act = trim($_POST["act"]);
if (isset($_GET["act"]))   $act = trim($_GET["act"]);

if (isset($act) && trim($act) != "") {
    if ($act == "save_to_transaksi") {
        if ($_SERVER["REQUEST_METHOD"] != "POST") {
            echo composeReply("ERROR", "[Routing ERROR] Terjadi kesalahan internal.");
            exit;
        }

        if (isset($_POST["loginToken"]) && trim($_POST["loginToken"]) != "")     $loginToken = trim($_POST["loginToken"]);
        if (!isset($loginToken)) {
            echo composeReply("ERROR", "Akses tidak dikenal", array("API_ACTION" => "LOGOUT"));
            exit;
        }

        $stmt = $gPDO->prepare("SELECT * FROM _users WHERE U_LOGIN_TOKEN = ?");
        $stmt->execute([$loginToken]);
        $loginData = $stmt->fetch(PDO::FETCH_OBJ);
        if (!$loginData) {
            echo composeReply("ERROR", "User tidak dikenal", array("API_ACTION" => "LOGOUT"));
            exit;
        }

        if (isset($_POST["t_kodebooking"]) && trim($_POST["t_kodebooking"]) != "") $t_kodebooking = trim($_POST["t_kodebooking"]);
        if (!isset($t_kodebooking)) {
            echo composeReply("ERROR", "Harap isikan Kode Booking");
            exit;
        }

        if (isset($_POST["t_status"]) && trim($_POST["t_status"]) != "") $t_status = trim($_POST["t_status"]);
        if (!isset($t_status)) {
            echo composeReply("ERROR", "Harap isikan Status Booking");
            exit;
        }

        if (isset($_POST["t_totalbiaya"]) && trim($_POST["t_totalbiaya"]) != "") $t_totalbiaya = trim($_POST["t_totalbiaya"]);
        if (!isset($t_totalbiaya)) {
            echo composeReply("ERROR", "Harap isikan Total Biaya");
            exit;
        }

        if (isset($_POST["t_date"]) && trim($_POST["t_date"]) != "") $t_date = trim($_POST["t_date"]);
        if (!isset($t_date)) {
            echo composeReply("ERROR", "Harap isikan tanggal ambil");
            exit;
        }

        if (isset($_POST["t_time"]) && trim($_POST["t_time"]) != "") $t_time = trim($_POST["t_time"]);
        if (!isset($t_time)) {
            echo composeReply("ERROR", "Harap isikan jam ambil");
            exit;
        }

        $gPDO->prepare("INSERT INTO rs_transaksi (t_date_book, t_time_book ,t_kodebooking, t_cust_id, t_cust_username, t_cust_ktp, t_status, t_totalbiaya, t_date, t_time) VALUES (?,?,?,?,?,?,?,?,?,?)")->execute([date("Y-m-d"), date("H-i-s"), $t_kodebooking, $loginData->{"U_ID"}, $loginData->{"U_NAME"}, $loginData->{"U_AUTHORITY_ID_1"}, $t_status, $t_totalbiaya, $t_date, $t_time]);
        $i_id = $gPDO->lastInsertId();
        if (isset($i_id) && $i_id > 0) {
            $stmt = $gPDO->query("SELECT U_FCM_TOKEN FROM  _users WHERE U_GROUP_ROLE = 'admin' OR U_GROUP_ROLE = 'superadmin'");
            $userData = $stmt->fetchAll(PDO::FETCH_OBJ);
            if (!$userData) {
                echo composeReply("SUCCESS", "User Admin Tidak Ada");
                exit;
            } else {
                foreach ($userData as $userDatas) {
                    //functSend
                    $token = $userDatas->{"U_FCM_TOKEN"};
                    $notificationTitle = "Transaksi Booking Baru";
                    $notificationBody = "Hai, ada transaksi booking baru dari " . $loginData->{"U_NAME"} . ", buruan cek";
                    $payloadTitle = "payloadTitle";
                    $payloadMessage = "payloadMessage";
                    sendAndroidPushNotificationToUser($token, $notificationTitle, $notificationBody, $payloadTitle, $payloadMessage);
                }
                echo composeReply("SUCCESS", "Admin User", $userData);
                exit;
            }
            echo composeReply("SUCCESS", "Booking telah disimpan");
            exit;
        } else {
            echo composeReply("ERROR", "Gagal menyimpan booking");
            exit;
        }
    }

    if ($act == "save_to_item_transaksi") {
        if ($_SERVER["REQUEST_METHOD"] != "POST") {
            echo composeReply("ERROR", "[Routing ERROR] Terjadi kesalahan internal.");
            exit;
        }

        if (isset($_POST["loginToken"]) && trim($_POST["loginToken"]) != "")     $loginToken = trim($_POST["loginToken"]);
        if (!isset($loginToken)) {
            echo composeReply("ERROR", "Akses tidak dikenal", array("API_ACTION" => "LOGOUT"));
            exit;
        }

        $stmt = $gPDO->prepare("SELECT * FROM _users WHERE U_LOGIN_TOKEN = ?");
        $stmt->execute([$loginToken]);
        $loginData = $stmt->fetch(PDO::FETCH_OBJ);
        if (!$loginData) {
            echo composeReply("ERROR", "User tidak dikenal", array("API_ACTION" => "LOGOUT"));
            exit;
        }

        if (isset($_POST["t_kodebooking"]) && trim($_POST["t_kodebooking"]) != "") $t_kodebooking = trim($_POST["t_kodebooking"]);
        if (!isset($t_kodebooking)) {
            echo composeReply("ERROR", "Harap isikan Kode Booking");
            exit;
        }

        if (isset($_POST["i_status"]) && trim($_POST["i_status"]) != "") $i_status = trim($_POST["i_status"]);
        if (!isset($i_status)) {
            echo composeReply("ERROR", "Harap isikan status item");
            exit;
        }

        if (isset($_POST["i_kategori"]) && trim($_POST["i_kategori"]) != "") $i_kategori = trim($_POST["i_kategori"]);
        if (!isset($i_kategori)) {
            echo composeReply("ERROR", "Harap isikan kategori item");
            exit;
        }

        if (isset($_POST["i_merk"]) && trim($_POST["i_merk"]) != "") $i_merk = trim($_POST["i_merk"]);
        if (!isset($i_merk)) {
            echo composeReply("ERROR", "Harap isikan merk item");
            exit;
        }

        if (isset($_POST["i_seri"]) && trim($_POST["i_seri"]) != "") $i_seri = trim($_POST["i_seri"]);
        if (!isset($i_seri)) {
            echo composeReply("ERROR", "Harap isikan seri item");
            exit;
        }

        if (isset($_POST["i_harga"]) && trim($_POST["i_harga"]) != "") $i_harga = trim($_POST["i_harga"]);
        if (!isset($i_harga)) {
            echo composeReply("ERROR", "Harap isikan harga item");
            exit;
        }

        if (isset($_POST["i_foto"]) && trim($_POST["i_foto"]) != "") $i_foto = trim($_POST["i_foto"]);
        if (!isset($i_foto)) {
            echo composeReply("ERROR", "Harap isikan foto item");
            exit;
        }

        if (isset($_POST["i_deskripsi"]) && trim($_POST["i_deskripsi"]) != "") $i_deskripsi = trim($_POST["i_deskripsi"]);
        if (!isset($i_deskripsi)) {
            echo composeReply("ERROR", "Harap isikan deskripsi item");
            exit;
        }

        if (isset($_POST["i_durasi"]) && trim($_POST["i_durasi"]) != "") $i_durasi = trim($_POST["i_durasi"]);
        if (!isset($i_durasi)) {
            echo composeReply("ERROR", "Harap isikan Durasi Booking");
            exit;
        }

        if (isset($_POST["i_harga_total"]) && trim($_POST["i_harga_total"]) != "") $i_harga_total = trim($_POST["i_harga_total"]);
        if (!isset($i_harga_total)) {
            echo composeReply("ERROR", "Harap isikan Harga total item");
            exit;
        }

        $gPDO->prepare("INSERT INTO rs_transaksi_item (t_kodebooking, i_status, i_kategori, i_merk, i_seri, i_harga, i_foto, i_deskripsi, i_durasi, i_harga_total) VALUES (?,?,?,?,?,?,?,?,?,?)")->execute([$t_kodebooking, $i_status, $i_kategori, $i_merk, $i_seri, $i_harga, $i_foto, $i_deskripsi, $i_durasi, $i_harga_total,]);
        $i_id = $gPDO->lastInsertId();
        if (isset($i_id) && $i_id > 0) {
            echo composeReply("SUCCESS", "Item Booking telah disimpan");
            exit;
        } else {
            echo composeReply("ERROR", "Gagal menyimpan Item Booking");
            exit;
        }
    }

    if ($act == "update_status_booking") {
        if ($_SERVER["REQUEST_METHOD"] != "POST") {
            echo composeReply("ERROR", "[Routing ERROR] Terjadi kesalahan internal.");
            exit;
        }

        if (isset($_POST["loginToken"]) && trim($_POST["loginToken"]) != "")     $loginToken = trim($_POST["loginToken"]);
        if (!isset($loginToken)) {
            echo composeReply("ERROR", "Akses tidak dikenal", array("API_ACTION" => "LOGOUT"));
            exit;
        }

        $stmt = $gPDO->prepare("SELECT * FROM _users WHERE U_LOGIN_TOKEN = ?");
        $stmt->execute([$loginToken]);
        $loginData = $stmt->fetch(PDO::FETCH_OBJ);
        if (!$loginData) {
            echo composeReply("ERROR", "User tidak dikenal", array("API_ACTION" => "LOGOUT"));
            exit;
        }
        $cekAdmin = $loginData->{"U_GROUP_ROLE"};
        if ($cekAdmin == 'customer') {
            echo composeReply("ERROR", "Anda tidak memiliki akses", array("API_ACTION" => "LOGOUT"));
            exit;
        }

        if (isset($_POST["t_kodebooking"]) && trim($_POST["t_kodebooking"]) != "") $t_kodebooking = trim($_POST["t_kodebooking"]);
        if (!isset($t_kodebooking)) {
            echo composeReply("ERROR", "Harap isikan Kode Booking");
            exit;
        }

        if (isset($_POST["t_status"]) && trim($_POST["t_status"]) != "") $t_status = trim($_POST["t_status"]);
        if (!isset($t_status)) {
            echo composeReply("ERROR", "Harap isikan Status Booking");
            exit;
        }


        $gPDO->prepare("UPDATE rs_transaksi SET t_status = ? WHERE t_kodebooking = ?")->execute([$t_status, $t_kodebooking]);
        $gPDO->lastInsertId();
        if (isset($gPDO)) {
            $gPDO->prepare("UPDATE rs_transaksi_item SET i_status = ? WHERE t_kodebooking = ?")->execute([$t_status, $t_kodebooking]);
            $gPDO->lastInsertId();
            if (isset($gPDO)) {
                echo composeReply("SUCCESS", "Perubahan status booking telah disimpan");
                exit;
            } else {
                echo composeReply("ERROR", "Gagal menyimpan perubahan status booking");
                exit;
            }
        } else {
            echo composeReply("ERROR", "Gagal menyimpan perubahan status booking");
            exit;
        }
    }

    if ($act == "get_header_transaksi") {
        if ($_SERVER["REQUEST_METHOD"] != "POST") {
            echo composeReply("ERROR", "[Routing ERROR] Internal error.");
            exit;
        }

        if (!isset($_POST["loginToken"]) || trim($_POST["loginToken"]) == "") {
            echo composeReply("ERROR", "Silahkan login dahulu untuk mengakses fitur ini");
            exit;
        }
        $loginToken = trim($_POST["loginToken"]);

        $stmt = $gPDO->prepare("SELECT * FROM _users WHERE U_LOGIN_TOKEN = ?");
        $stmt->execute([$loginToken]);
        $loginData = $stmt->fetch(PDO::FETCH_OBJ);
        if (!$loginData) {
            echo composeReply("ERROR", "User tidak dikenal", array("API_ACTION" => "LOGOUT"));
            exit;
        }
        // $cekAdmin = $loginData->{"U_GROUP_ROLE"};
        // if ($cekAdmin == 'customer') {
        //     echo composeReply("ERROR", "Anda tidak memiliki akses", array("API_ACTION" => "LOGOUT"));
        //     exit;
        // }

        $t_cust_id = trim($_POST["t_cust_id"]);
        if (!isset($_POST["t_cust_id"]) || trim($_POST["t_cust_id"]) == "") {
            //Get User All
            $stmt = $gPDO->query("SELECT * FROM rs_transaksi");
            $userData = $stmt->fetchAll(PDO::FETCH_OBJ);
            if (!$userData) {
                echo composeReply("ERROR", "Transaksi Tidak Dimukan");
                exit;
            } else {
                echo composeReply("SUCCESS", "All Transaksi", $userData);
                exit;
            }
        } else if (!isset($_POST["t_cust_id"]) || trim($_POST["t_cust_id"]) == $t_cust_id) {
            $stmt = $gPDO->prepare("SELECT * FROM rs_transaksi WHERE t_cust_id = ?");
            $stmt->execute([$t_cust_id]);
            $userData = $stmt->fetchAll(PDO::FETCH_OBJ);
            if (!$userData) {
                echo composeReply("ERROR", "Transaksi Tidak Dimukan");
                exit;
            } else {
                echo composeReply("SUCCESS", "All Header By User ID", $userData);
                exit;
            }
        }
    }

    if ($act == "get_item_transaksi") {
        if ($_SERVER["REQUEST_METHOD"] != "POST") {
            echo composeReply("ERROR", "[Routing ERROR] Internal error.");
            exit;
        }

        if (!isset($_POST["loginToken"]) || trim($_POST["loginToken"]) == "") {
            echo composeReply("ERROR", "Silahkan login dahulu untuk mengakses fitur ini");
            exit;
        }
        $loginToken = trim($_POST["loginToken"]);

        $stmt = $gPDO->prepare("SELECT * FROM _users WHERE U_LOGIN_TOKEN = ?");
        $stmt->execute([$loginToken]);
        $loginData = $stmt->fetch(PDO::FETCH_OBJ);
        if (!$loginData) {
            echo composeReply("ERROR", "User tidak dikenal", array("API_ACTION" => "LOGOUT"));
            exit;
        }
        // $cekAdmin = $loginData->{"U_GROUP_ROLE"};
        // if ($cekAdmin == 'customer') {
        //     echo composeReply("ERROR", "Anda tidak memiliki akses", array("API_ACTION" => "LOGOUT"));
        //     exit;
        // }

        $t_kodebooking = trim($_POST["t_kodebooking"]);
        if (!isset($_POST["t_kodebooking"]) || trim($_POST["t_kodebooking"]) == "") {
            //Get User All
            $stmt = $gPDO->query("SELECT * FROM rs_transaksi_item");
            $userData = $stmt->fetchAll(PDO::FETCH_OBJ);
            if (!$userData) {
                echo composeReply("ERROR", "Transaksi Tidak Dimukan");
                exit;
            } else {
                echo composeReply("SUCCESS", "All Item Transaksi", $userData);
                exit;
            }
        } else if (!isset($_POST["t_kodebooking"]) || trim($_POST["t_kodebooking"]) == $t_kodebooking) {
            $stmt = $gPDO->prepare("SELECT * FROM rs_transaksi_item WHERE t_kodebooking = ?");
            $stmt->execute([$t_kodebooking]);
            $userData = $stmt->fetchAll(PDO::FETCH_OBJ);
            if (!$userData) {
                echo composeReply("ERROR", "Transaksi Tidak Dimukan");
                exit;
            } else {
                echo composeReply("SUCCESS", "All Item By User ID", $userData);
                exit;
            }
        }
    }

    if ($act == "create_bukti_booking") {
        if ($_SERVER["REQUEST_METHOD"] != "POST") {
            echo composeReply("ERROR", "[Routing ERROR] Internal error.");
            exit;
        }

        if (!isset($_POST["loginToken"]) || trim($_POST["loginToken"]) == "") {
            echo composeReply("ERROR", "Silahkan login dahulu untuk mengakses fitur ini");
            exit;
        }
        $loginToken = trim($_POST["loginToken"]);

        if (!isset($_POST["t_kodebooking"]) || trim($_POST["t_kodebooking"]) == "") {
            echo composeReply("ERROR", "Silahkan login dahulu untuk mengakses fitur ini");
            exit;
        }
        $t_kodebooking = trim($_POST["t_kodebooking"]);

        $stmt = $gPDO->prepare("SELECT * FROM _users WHERE U_LOGIN_TOKEN = ?");
        $stmt->execute([$loginToken]);
        $loginData = $stmt->fetch(PDO::FETCH_OBJ);
        if (!$loginData) {
            echo composeReply("ERROR", "User tidak dikenal", array("API_ACTION" => "LOGOUT"));
            exit;
        }

        if (isset($_FILES["uploadFile"])) {
            $fileName = $_FILES['uploadFile']['name'];
            $fileSize = $_FILES['uploadFile']['size'];
            $fileTmp = $_FILES['uploadFile']['tmp_name'];
            $fileType = $_FILES['uploadFile']['type'];
            $fileError = $_FILES['uploadFile']['error'];

            $a = explode(".", $_FILES["uploadFile"]["name"]);
            $fileExt = strtolower(end($a));

            if (isset($fileError) && $fileError > 0) {
                $FILE_UPLOAD_ERROR_INFO = array(
                    0 => 'There is no error, the file uploaded with success',
                    1 => 'Ukuran file terlalu besar'/*'The uploaded file exceeds the upload_max_filesize directive in php.ini'*/,
                    2 => 'Ukuran file terlalu besar'/*'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form'*/,
                    3 => 'Terjadi gangguan jaringan sehingga data terpotong'/*'The uploaded file was only partially uploaded'*/,
                    4 => 'Tidak ada file yang diunggah',
                    6 => 'Temporary folder tidak tersedia',
                    7 => 'Gagal menyimpan data',
                    8 => /*'A PHP extension stopped the file upload.'*/ 'Terjadi kesalahan internal di server',
                );

                echo composeReply("ERROR", $FILE_UPLOAD_ERROR_INFO[$fileError]);
                exit;
            }

            $arrFileExt = array("jpg", "jpeg", "png");
            if (isset($fileName) && trim($fileName) != "") {
                if (in_array($fileExt, $arrFileExt) === false) {
                    echo composeReply("ERROR", "Harap pilih format image yang sesuai");
                    exit;
                }

                $uploadFile = substr(md5(date("YmdHis")), 0, 5) . "." . $fileExt;
                if (move_uploaded_file($fileTmp, "../uploads/" . $uploadFile)) {
                    $gPDO->prepare("UPDATE rs_transaksi SET t_bukti_book = ? WHERE t_kodebooking = ?")->execute([$uploadFile, $t_kodebooking]);
                    echo composeReply("SUCCESS", "Upload bukti tanda jadi / dp berhasil di upload, admin akan segera konfirmasi ke anda", array(
                        "t_bukti_book" => $uploadFile
                    ));
                    exit;
                } else {
                    echo composeReply("ERROR", "Gagal menyimpan Foto bukti tanda jadi / dp");
                    exit;
                }
            } else {
                echo composeReply("ERROR", "Gagal menyimpan Foto bukti tanda jadi / dp");
                exit;
            }
        } else {
            echo composeReply("ERROR", "Harap upload file");
            exit;
        }
    }

    if ($act == "update_keterangan_booking") {
        if ($_SERVER["REQUEST_METHOD"] != "POST") {
            echo composeReply("ERROR", "[Routing ERROR] Terjadi kesalahan internal.");
            exit;
        }

        if (isset($_POST["loginToken"]) && trim($_POST["loginToken"]) != "")     $loginToken = trim($_POST["loginToken"]);
        if (!isset($loginToken)) {
            echo composeReply("ERROR", "Akses tidak dikenal", array("API_ACTION" => "LOGOUT"));
            exit;
        }

        $stmt = $gPDO->prepare("SELECT * FROM _users WHERE U_LOGIN_TOKEN = ?");
        $stmt->execute([$loginToken]);
        $loginData = $stmt->fetch(PDO::FETCH_OBJ);
        if (!$loginData) {
            echo composeReply("ERROR", "User tidak dikenal", array("API_ACTION" => "LOGOUT"));
            exit;
        }

        if (isset($_POST["t_kodebooking"]) && trim($_POST["t_kodebooking"]) != "") $t_kodebooking = trim($_POST["t_kodebooking"]);
        if (!isset($t_kodebooking)) {
            echo composeReply("ERROR", "Harap isikan Kode Booking");
            exit;
        }

        if (isset($_POST["t_keterangan"]) && trim($_POST["t_keterangan"]) != "") $t_keterangan = trim($_POST["t_keterangan"]);
        if (!isset($t_keterangan)) {
            echo composeReply("ERROR", "Harap isikan Status Booking");
            exit;
        }


        $gPDO->prepare("UPDATE rs_transaksi SET t_keterangan = ? WHERE t_kodebooking = ?")->execute([$t_keterangan, $t_kodebooking]);
        $gPDO->lastInsertId();
        if (isset($gPDO)) {
            $gPDO->prepare("UPDATE rs_transaksi SET t_status = ? WHERE t_kodebooking = ?")->execute(['Dibatalkan', $t_kodebooking]);
            $gPDO->lastInsertId();
            if (isset($gPDO)) {
                $gPDO->prepare("UPDATE rs_transaksi_item SET t_keterangan = ? WHERE t_kodebooking = ?")->execute([$t_keterangan, $t_kodebooking]);
                $gPDO->lastInsertId();
                if (isset($gPDO)) {
                    $gPDO->prepare("UPDATE rs_transaksi_item SET i_status = ? WHERE t_kodebooking = ?")->execute(['Dibatalkan', $t_kodebooking]);
                    $gPDO->lastInsertId();
                    if (isset($gPDO)) {
                        echo composeReply("SUCCESS", "Perubahan keterangan booking telah disimpan");
                        exit;
                    } else {
                        echo composeReply("ERROR", "Gagal menyimpan perubahan keterangan booking");
                        exit;
                    }
                } else {
                    echo composeReply("ERROR", "Gagal menyimpan perubahan keterangan booking");
                    exit;
                }
            } else {
                echo composeReply("ERROR", "Gagal menyimpan perubahan keterangan booking");
                exit;
            }
        } else {
            echo composeReply("ERROR", "Gagal menyimpan perubahan keterangan booking");
            exit;
        }
    }

    if ($act == "get_header_transaksi_bystatus") {
        if ($_SERVER["REQUEST_METHOD"] != "POST") {
            echo composeReply("ERROR", "[Routing ERROR] Internal error.");
            exit;
        }

        if (!isset($_POST["loginToken"]) || trim($_POST["loginToken"]) == "") {
            echo composeReply("ERROR", "Silahkan login dahulu untuk mengakses fitur ini");
            exit;
        }
        $loginToken = trim($_POST["loginToken"]);

        $stmt = $gPDO->prepare("SELECT * FROM _users WHERE U_LOGIN_TOKEN = ?");
        $stmt->execute([$loginToken]);
        $loginData = $stmt->fetch(PDO::FETCH_OBJ);
        if (!$loginData) {
            echo composeReply("ERROR", "User tidak dikenal", array("API_ACTION" => "LOGOUT"));
            exit;
        }
        // $cekAdmin = $loginData->{"U_GROUP_ROLE"};
        // if ($cekAdmin == 'customer') {
        //     echo composeReply("ERROR", "Anda tidak memiliki akses", array("API_ACTION" => "LOGOUT"));
        //     exit;
        // }

        if (!isset($_POST["statusTransaksi"]) || trim($_POST["statusTransaksi"]) == "") {
            echo composeReply("ERROR", "Status Transaksi tidak boleh kosong");
            exit;
        }
        $statusTransaksi = trim($_POST["statusTransaksi"]);

        if (trim($_POST["statusTransaksi"]) == "01") {
            $tStatus = "";
        } else if (trim($_POST["statusTransaksi"]) == "02") {
            $tStatus = "Menunggu Konfirmasi Admin";
        } else if (trim($_POST["statusTransaksi"]) == "03") {
            $tStatus = "Belum membayar tanda jadi / dp";
        } else if (trim($_POST["statusTransaksi"]) == "04") {
            $tStatus = "Produk Belum Diambil";
        } else if (trim($_POST["statusTransaksi"]) == "05") {
            $tStatus = "Booking Berlangsung";
        } else if (trim($_POST["statusTransaksi"]) == "06") {
            $tStatus = "Selesai";
        } else if (trim($_POST["statusTransaksi"]) == "07") {
            $tStatus = "Dibatalkan";
        } else if (trim($_POST["statusTransaksi"]) == "08") {
            $tStatus = "Lewat Batas Waktu";
        }

        $t_cust_id = trim($_POST["t_cust_id"]);
        if (!isset($_POST["t_cust_id"]) || trim($_POST["t_cust_id"]) == "") {
            //Get User All
            $stmt = $gPDO->query("SELECT * FROM rs_transaksi");
            $userData = $stmt->fetchAll(PDO::FETCH_OBJ);
            if (!$userData) {
                echo composeReply("ERROR", "Transaksi Tidak Dimukan");
                exit;
            } else {
                echo composeReply("SUCCESS", "All Transaksi", $userData);
                exit;
            }
        } else if (!isset($_POST["t_cust_id"]) || trim($_POST["t_cust_id"]) == $t_cust_id) {
            if ($tStatus == "") {
                $stmt = $gPDO->prepare("SELECT * FROM rs_transaksi WHERE t_cust_id = ?");
                $stmt->execute([$t_cust_id]);
                $userData = $stmt->fetchAll(PDO::FETCH_OBJ);
                if (!$userData) {
                    echo composeReply("ERROR", "Belum Ada Transaksi");
                    exit;
                } else {
                    echo composeReply("SUCCESS", "All Header By User ID and Status", $userData);
                    exit;
                }
            } else {
                $stmt = $gPDO->prepare("SELECT * FROM rs_transaksi WHERE t_cust_id = ? AND 	t_status = ?");
                $stmt->execute([$t_cust_id, $tStatus]);
                $userData = $stmt->fetchAll(PDO::FETCH_OBJ);
                if (!$userData) {
                    echo composeReply("ERROR", "Transaksi dengan status " . $tStatus . " tidak ditemukan");
                    exit;
                } else {
                    echo composeReply("SUCCESS", "All Header By User ID and Status", $userData);
                    exit;
                }
            }
        }
    }
} else {
    echo composeReply("ERROR", "[Routing ERROR] Terjadi kesalahan internal.");
    exit;
}
