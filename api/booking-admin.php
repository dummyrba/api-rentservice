<?php
define('_VALID_ACCESS', TRUE);
include "../middle/conn.php";
include "../middle/functions.php";

header("Content-type: application/json");

$errMsg = "";

$result = "";
//
if (isset($_POST["act"]))  $act = trim($_POST["act"]);
if (isset($_GET["act"]))   $act = trim($_GET["act"]);

if (isset($act) && trim($act) != "") {
    if ($act == "get_header_transaksi_bystatus") {
        if ($_SERVER["REQUEST_METHOD"] != "POST") {
            echo composeReply("ERROR", "[Routing ERROR] Internal error.");
            exit;
        }

        if (!isset($_POST["loginToken"]) || trim($_POST["loginToken"]) == "") {
            echo composeReply("ERROR", "Silahkan login dahulu untuk mengakses fitur ini");
            exit;
        }
        $loginToken = trim($_POST["loginToken"]);

        $stmt = $gPDO->prepare("SELECT * FROM _users WHERE U_LOGIN_TOKEN = ?");
        $stmt->execute([$loginToken]);
        $loginData = $stmt->fetch(PDO::FETCH_OBJ);
        if (!$loginData) {
            echo composeReply("ERROR", "User tidak dikenal", array("API_ACTION" => "LOGOUT"));
            exit;
        }
        $cekAdmin = $loginData->{"U_GROUP_ROLE"};
        if ($cekAdmin == 'customer') {
            echo composeReply("ERROR", "Anda tidak memiliki akses", array("API_ACTION" => "LOGOUT"));
            exit;
        }

        if (!isset($_POST["statusTransaksi"]) || trim($_POST["statusTransaksi"]) == "") {
            echo composeReply("ERROR", "Status Transaksi tidak boleh kosong");
            exit;
        }
        $statusTransaksi = trim($_POST["statusTransaksi"]);

        if (trim($_POST["statusTransaksi"]) == "01") {
            $tStatus = "";
        } else if (trim($_POST["statusTransaksi"]) == "02") {
            $tStatus = "Menunggu Konfirmasi Admin";
        } else if (trim($_POST["statusTransaksi"]) == "03") {
            $tStatus = "Belum membayar tanda jadi / dp";
        } else if (trim($_POST["statusTransaksi"]) == "04") {
            $tStatus = "Produk Belum Diambil";
        } else if (trim($_POST["statusTransaksi"]) == "05") {
            $tStatus = "Booking Berlangsung";
        } else if (trim($_POST["statusTransaksi"]) == "06") {
            $tStatus = "Selesai";
        } else if (trim($_POST["statusTransaksi"]) == "07") {
            $tStatus = "Dibatalkan";
        } else if (trim($_POST["statusTransaksi"]) == "08") {
            $tStatus = "Lewat Batas Waktu";
        }

        if ($tStatus == "") {
            $stmt = $gPDO->query("SELECT * FROM rs_transaksi");
            $userData = $stmt->fetchAll(PDO::FETCH_OBJ);
            if (!$userData) {
                echo composeReply("SUCCESS", "Belum Ada Transaksi");
                exit;
            } else {
                echo composeReply("SUCCESS", "All Header", $userData);
                exit;
            }
        } else {
            $stmt = $gPDO->prepare("SELECT * FROM rs_transaksi WHERE t_status = ?");
            $stmt->execute([$tStatus]);
            $userData = $stmt->fetchAll(PDO::FETCH_OBJ);
            if (!$userData) {
                echo composeReply("SUCCESS", "Transaksi dengan status " . $tStatus . " tidak ditemukan");
                exit;
            } else {
                echo composeReply("SUCCESS", "All Header By Status", $userData);
                exit;
            }
        }
    }

    if ($act == "get_item_transaksi") {
        if ($_SERVER["REQUEST_METHOD"] != "POST") {
            echo composeReply("ERROR", "[Routing ERROR] Internal error.");
            exit;
        }

        if (!isset($_POST["loginToken"]) || trim($_POST["loginToken"]) == "") {
            echo composeReply("ERROR", "Silahkan login dahulu untuk mengakses fitur ini");
            exit;
        }
        $loginToken = trim($_POST["loginToken"]);

        $stmt = $gPDO->prepare("SELECT * FROM _users WHERE U_LOGIN_TOKEN = ?");
        $stmt->execute([$loginToken]);
        $loginData = $stmt->fetch(PDO::FETCH_OBJ);
        if (!$loginData) {
            echo composeReply("ERROR", "User tidak dikenal", array("API_ACTION" => "LOGOUT"));
            exit;
        }
        $cekAdmin = $loginData->{"U_GROUP_ROLE"};
        if ($cekAdmin == 'customer') {
            echo composeReply("ERROR", "Anda tidak memiliki akses", array("API_ACTION" => "LOGOUT"));
            exit;
        }

        $t_kodebooking = trim($_POST["t_kodebooking"]);
        if (!isset($_POST["t_kodebooking"]) || trim($_POST["t_kodebooking"]) == "") {
            //Get User All
            $stmt = $gPDO->query("SELECT * FROM rs_transaksi_item");
            $userData = $stmt->fetchAll(PDO::FETCH_OBJ);
            if (!$userData) {
                echo composeReply("ERROR", "Transaksi Tidak Dimukan");
                exit;
            } else {
                echo composeReply("SUCCESS", "All Item Transaksi", $userData);
                exit;
            }
        } else if (!isset($_POST["t_kodebooking"]) || trim($_POST["t_kodebooking"]) == $t_kodebooking) {
            $stmt = $gPDO->prepare("SELECT * FROM rs_transaksi_item WHERE t_kodebooking = ?");
            $stmt->execute([$t_kodebooking]);
            $userData = $stmt->fetchAll(PDO::FETCH_OBJ);
            if (!$userData) {
                echo composeReply("ERROR", "Transaksi Tidak Dimukan");
                exit;
            } else {
                echo composeReply("SUCCESS", "All Item By User ID", $userData);
                exit;
            }
        }
    }

    if ($act == "get_header_transaksi_by_kode_booking") {
        if ($_SERVER["REQUEST_METHOD"] != "POST") {
            echo composeReply("ERROR", "[Routing ERROR] Internal error.");
            exit;
        }

        if (!isset($_POST["loginToken"]) || trim($_POST["loginToken"]) == "") {
            echo composeReply("ERROR", "Silahkan login dahulu untuk mengakses fitur ini");
            exit;
        }
        $loginToken = trim($_POST["loginToken"]);

        $stmt = $gPDO->prepare("SELECT * FROM _users WHERE U_LOGIN_TOKEN = ?");
        $stmt->execute([$loginToken]);
        $loginData = $stmt->fetch(PDO::FETCH_OBJ);
        if (!$loginData) {
            echo composeReply("ERROR", "User tidak dikenal", array("API_ACTION" => "LOGOUT"));
            exit;
        }
        $cekAdmin = $loginData->{"U_GROUP_ROLE"};
        if ($cekAdmin == 'customer') {
            echo composeReply("ERROR", "Anda tidak memiliki akses", array("API_ACTION" => "LOGOUT"));
            exit;
        }

        if (!isset($_POST["kodeBooking"]) || trim($_POST["kodeBooking"]) == "") {
            echo composeReply("ERROR", "Kode booking tidak boleh kosong");
            exit;
        }
        $kodeBooking = trim($_POST["kodeBooking"]);

        $stmt = $gPDO->prepare("SELECT * FROM rs_transaksi WHERE t_kodebooking = ?");
        $stmt->execute([$kodeBooking]);
        $userData = $stmt->fetchAll(PDO::FETCH_OBJ);
        if (!$userData) {
            echo composeReply("ERROR", "Transaksi dengan kode booking " . $kodeBooking . " tidak ditemukan");
            exit;
        } else {
            echo composeReply("SUCCESS", "All Header By kode booking " . $kodeBooking, $userData);
            exit;
        }
    }

    if ($act == "update_status_booking_selesai") {
        if ($_SERVER["REQUEST_METHOD"] != "POST") {
            echo composeReply("ERROR", "[Routing ERROR] Terjadi kesalahan internal.");
            exit;
        }

        if (isset($_POST["loginToken"]) && trim($_POST["loginToken"]) != "")     $loginToken = trim($_POST["loginToken"]);
        if (!isset($loginToken)) {
            echo composeReply("ERROR", "Akses tidak dikenal", array("API_ACTION" => "LOGOUT"));
            exit;
        }

        $stmt = $gPDO->prepare("SELECT * FROM _users WHERE U_LOGIN_TOKEN = ?");
        $stmt->execute([$loginToken]);
        $loginData = $stmt->fetch(PDO::FETCH_OBJ);
        if (!$loginData) {
            echo composeReply("ERROR", "User tidak dikenal", array("API_ACTION" => "LOGOUT"));
            exit;
        }
        $cekAdmin = $loginData->{"U_GROUP_ROLE"};
        if ($cekAdmin == 'customer') {
            echo composeReply("ERROR", "Anda tidak memiliki akses", array("API_ACTION" => "LOGOUT"));
            exit;
        }

        if (isset($_POST["t_kodebooking"]) && trim($_POST["t_kodebooking"]) != "") $t_kodebooking = trim($_POST["t_kodebooking"]);
        if (!isset($t_kodebooking)) {
            echo composeReply("ERROR", "Harap isikan Kode Booking");
            exit;
        }

        if (isset($_POST["t_status"]) && trim($_POST["t_status"]) != "") $t_status = trim($_POST["t_status"]);
        if (!isset($t_status)) {
            echo composeReply("ERROR", "Harap isikan Status Booking");
            exit;
        }

        //Count data berdasarkan kodeBooking
        $stmt = $gPDO->prepare("SELECT COUNT(*) FROM rs_transaksi_item WHERE t_kodebooking = ?");
        $stmt->execute([$t_kodebooking]);
        $data = $stmt->fetchColumn();

        //Count data berdasarkan kodeBooking dan statusSelesai
        $stmt = $gPDO->prepare("SELECT COUNT(*) FROM rs_transaksi_item WHERE t_kodebooking = ? AND i_status = ?");
        $stmt->execute([$t_kodebooking, $t_status]);
        $dataSelesai = $stmt->fetchColumn();

        //check valid data
        if ($data == $dataSelesai) {
            // echo composeReply("SUCCESS", "ini Jika semua item selesai karena banyak data " . $data . " sedangkan yang sudah selesai " . $dataSelesai);
            $gPDO->prepare("UPDATE rs_transaksi SET t_status = ? WHERE t_kodebooking = ?")->execute([$t_status, $t_kodebooking]);
            $gPDO->lastInsertId();
            if (isset($gPDO)) {
                $gPDO->prepare("UPDATE rs_transaksi_item SET i_status = ? WHERE t_kodebooking = ?")->execute([$t_status, $t_kodebooking]);
                $gPDO->lastInsertId();
                if (isset($gPDO)) {
                    echo composeReply("SUCCESS", "Perubahan status booking telah disimpan");
                    exit;
                } else {
                    echo composeReply("ERROR", "Gagal menyimpan perubahan status booking");
                    exit;
                }
            } else {
                echo composeReply("ERROR", "Gagal menyimpan perubahan status booking");
                exit;
            }
        } else {
            echo composeReply("ERROR", "Masih terdapat item yang statusnya belum selesai. Banyak item booking " . $data . " yang sudah selesai " . $dataSelesai);
        }
    }

    if ($act == "update_status_item_booking") {
        if ($_SERVER["REQUEST_METHOD"] != "POST") {
            echo composeReply("ERROR", "[Routing ERROR] Terjadi kesalahan internal.");
            exit;
        }

        if (isset($_POST["loginToken"]) && trim($_POST["loginToken"]) != "")     $loginToken = trim($_POST["loginToken"]);
        if (!isset($loginToken)) {
            echo composeReply("ERROR", "Akses tidak dikenal", array("API_ACTION" => "LOGOUT"));
            exit;
        }

        $stmt = $gPDO->prepare("SELECT * FROM _users WHERE U_LOGIN_TOKEN = ?");
        $stmt->execute([$loginToken]);
        $loginData = $stmt->fetch(PDO::FETCH_OBJ);
        if (!$loginData) {
            echo composeReply("ERROR", "User tidak dikenal", array("API_ACTION" => "LOGOUT"));
            exit;
        }
        $cekAdmin = $loginData->{"U_GROUP_ROLE"};
        if ($cekAdmin == 'customer') {
            echo composeReply("ERROR", "Anda tidak memiliki akses", array("API_ACTION" => "LOGOUT"));
            exit;
        }

        if (isset($_POST["t_i_id"]) && trim($_POST["t_i_id"]) != "") $t_i_id = trim($_POST["t_i_id"]);
        if (!isset($t_i_id)) {
            echo composeReply("ERROR", "Harap isikan id item Booking");
            exit;
        }

        if (isset($_POST["t_status"]) && trim($_POST["t_status"]) != "") $t_status = trim($_POST["t_status"]);
        if (!isset($t_status)) {
            echo composeReply("ERROR", "Harap isikan Status Item Booking");
            exit;
        }

        $gPDO->prepare("UPDATE rs_transaksi_item SET i_status = ? WHERE t_i_id = ?")->execute([$t_status, $t_i_id]);
        $gPDO->lastInsertId();
        if (isset($gPDO)) {
            echo composeReply("SUCCESS", "Perubahan status item booking telah disimpan");
            exit;
        } else {
            echo composeReply("ERROR", "Gagal menyimpan perubahan status item booking");
            exit;
        }
    }
} else {
    echo composeReply("ERROR", "[Routing ERROR] Terjadi kesalahan internal.");
    exit;
}
