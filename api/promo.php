<?php
define('_VALID_ACCESS', TRUE);
include "../middle/conn.php";
include "../middle/functions.php";

header("Content-type: application/json");

$errMsg = "";

$result = "";
//
if (isset($_POST["act"]))  $act = trim($_POST["act"]);
if (isset($_GET["act"]))   $act = trim($_GET["act"]);

if (isset($act) && trim($act) != "") {
	if ($act == "upload_promo") {
            if ($_SERVER["REQUEST_METHOD"] != "POST") {
                echo composeReply("ERROR", "[Routing ERROR] Internal error.");
                exit;
            }

            if (!isset($_POST["loginToken"]) || trim($_POST["loginToken"]) == "") {
                echo composeReply("ERROR", "Silahkan login dahulu untuk mengakses fitur ini");
                exit;
            }
            $loginToken = trim($_POST["loginToken"]);

            $stmt = $gPDO->prepare("SELECT * FROM _users WHERE U_LOGIN_TOKEN = ?");
            $stmt->execute([$loginToken]);
            $loginData = $stmt->fetch(PDO::FETCH_OBJ);
            if(!$loginData) {
                echo composeReply("ERROR", "User tidak dikenal", array("API_ACTION" => "LOGOUT"));
                exit;
            }

            if(isset($_FILES["uploadFile"])) {
                $fileName = $_FILES['uploadFile']['name'];
                $fileSize = $_FILES['uploadFile']['size'];
                $fileTmp = $_FILES['uploadFile']['tmp_name'];
                $fileType = $_FILES['uploadFile']['type'];
                $fileError = $_FILES['uploadFile']['error'];

                $a = explode(".", $_FILES["uploadFile"]["name"]);
                $fileExt = strtolower(end($a));

                if (isset($fileError) && $fileError > 0) {
                    $FILE_UPLOAD_ERROR_INFO = array(
                        0 => 'There is no error, the file uploaded with success',
                        1 => 'Ukuran file terlalu besar'/*'The uploaded file exceeds the upload_max_filesize directive in php.ini'*/,
                        2 => 'Ukuran file terlalu besar'/*'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form'*/,
                        3 => 'Terjadi gangguan jaringan sehingga data terpotong'/*'The uploaded file was only partially uploaded'*/,
                        4 => 'Tidak ada file yang diunggah',
                        6 => 'Temporary folder tidak tersedia',
                        7 => 'Gagal menyimpan data',
                        8 => /*'A PHP extension stopped the file upload.'*/'Terjadi kesalahan internal di server',
                    );

                    echo composeReply("ERROR", $FILE_UPLOAD_ERROR_INFO[$fileError]);
                    exit;
                }

                $arrFileExt = array("jpg", "jpeg", "png");
                if (isset($fileName) && trim($fileName) != "") {
                    if (in_array($fileExt, $arrFileExt) === false) {
                        echo composeReply("ERROR", "Harap pilih format image yang sesuai");
                        exit;
                    }

                    $uploadFile = substr(md5(date("YmdHis")), 0, 5) . "." . $fileExt;
                    if (move_uploaded_file($fileTmp, "../uploads/" . $uploadFile)) {
                        $gPDO->prepare("INSERT INTO image_promo (image_path) VALUES (?)")->execute([$uploadFile]);
                        echo composeReply("SUCCESS", "Foto Promo Berhasil di Upload");
                        exit;
                    } 
                    else {
                        echo composeReply("ERROR", "Gagal menyimpan Foto Promo");
                        exit;
                    }
                } 
                else {
                    echo composeReply("ERROR", "Gagal menyimpan Foto Promo");
                    exit;
                }
            }else {
                echo composeReply("ERROR", "Harap upload file");
                exit;
            }
    }

    if($act == "delete_promo") {
        if($_SERVER["REQUEST_METHOD"] != "POST") {
            echo composeReply("ERROR", "[Routing ERROR] Internal error.");
            exit;
        }

        if(isset($_POST["loginToken"]) && trim($_POST["loginToken"]) != "")     $loginToken = trim($_POST["loginToken"]);
        if(!isset($loginToken)) {
            echo composeReply("ERROR", "Akses tidak dikenal", array("API_ACTION" => "LOGOUT"));
            exit;
        }

        $stmt = $gPDO->prepare("SELECT * FROM _users WHERE U_LOGIN_TOKEN = ?");
        $stmt->execute([$loginToken]);
        $userData = $stmt->fetch(PDO::FETCH_OBJ);
        if(!$userData) {
            echo composeReply("ERROR", "User tidak dikenal", array("API_ACTION" => "LOGOUT"));
            exit;
        }

        if(isset($_POST["id"]) && trim($_POST["id"]) != "") $id = trim(strtoupper($_POST["id"]));
        if(!isset($id)) {
            echo composeReply("ERROR", "Parameter tidak lengkap");
            exit;
        }

        $stmt = $gPDO->prepare("SELECT * FROM image_promo WHERE id = ?");
        $stmt->execute([$id]);
        $orderData = $stmt->fetch(PDO::FETCH_OBJ);
        if(!$orderData) {
            echo composeReply("ERROR", "Promo tidak dikenal");
            exit;
        }

        $gPDO->prepare("DELETE FROM image_promo WHERE id = ?")->execute([$id]);

        echo composeReply("SUCCESS", "Promo berhasil di hapus");
        exit;
    }

    if($act == "get_promo") {
        if ($_SERVER["REQUEST_METHOD"] != "POST") {
                echo composeReply("ERROR", "[Routing ERROR] Internal error.");
                exit;
            }

            // if (!isset($_POST["loginToken"]) || trim($_POST["loginToken"]) == "") {
            //     echo composeReply("ERROR", "Silahkan login dahulu untuk mengakses fitur ini");
            //     exit;
            // }
            // $loginToken = trim($_POST["loginToken"]);

            // $stmt = $gPDO->prepare("SELECT * FROM _users WHERE U_LOGIN_TOKEN = ?");
            // $stmt->execute([$loginToken]);
            // $loginData = $stmt->fetch(PDO::FETCH_OBJ);
            // if(!$loginData) {
            //     echo composeReply("ERROR", "User tidak dikenal", array("API_ACTION" => "LOGOUT"));
            //     exit;
            // }

            $stmt = $gPDO->query("SELECT * FROM image_promo");
            $userData = $stmt->fetchAll(PDO::FETCH_OBJ);
            if (!$userData) {
                echo composeReply("SUCCESS", "User Tidak Dimukan");
                exit;
            } 
            else {
                echo composeReply("SUCCESS", "All User", $userData);
                exit;
            }
    }

} 
else {
    echo composeReply("ERROR", "[Routing ERROR] Terjadi kesalahan internal.");
    exit;
}