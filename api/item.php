<?php
define('_VALID_ACCESS', TRUE);
include "../middle/conn.php";
include "../middle/functions.php";

header("Content-type: application/json");

$errMsg = "";

$result = "";
//
if (isset($_POST["act"]))  $act = trim($_POST["act"]);
if (isset($_GET["act"]))   $act = trim($_GET["act"]);

if (isset($act) && trim($act) != "") {
    if ($act == "get_item_bycategory") {
        if ($_SERVER["REQUEST_METHOD"] != "POST") {
            echo composeReply("ERROR", "[Routing ERROR] Internal error.");
            exit;
        }
        // $cekAdmin = $loginData->{"U_GROUP_ROLE"};
        // if ($cekAdmin == 'customer') {
        //     echo composeReply("ERROR", "Anda tidak memiliki akses", array("API_ACTION" => "LOGOUT"));
        //     exit;
        // }

        $item = trim($_POST["item"]);
        if (!isset($_POST["item"]) || trim($_POST["item"]) == "") {
            //Get User All
            $stmt = $gPDO->query("SELECT * FROM rs_item");
            $userData = $stmt->fetchAll(PDO::FETCH_OBJ);
            if (!$userData) {
                echo composeReply("ERROR", "Item Tidak Dimukan");
                exit;
            } else {
                echo composeReply("SUCCESS", "All Item", $userData);
                exit;
            }
        } else if (!isset($_POST["item"]) || trim($_POST["item"]) == $item) {
            $stmt = $gPDO->prepare("SELECT * FROM rs_item WHERE i_kategori = ?");
            $stmt->execute([$item]);
            $userData = $stmt->fetchAll(PDO::FETCH_OBJ);
            if (!$userData) {
                echo composeReply("SUCCESS", $item . " Tidak Ada");
                exit;
            } else {
                echo composeReply("SUCCESS", "All Item By Kategori", $userData);
                exit;
            }
        }
    }

    if ($act == "save_update_item") {
        if ($_SERVER["REQUEST_METHOD"] != "POST") {
            echo composeReply("ERROR", "[Routing ERROR] Terjadi kesalahan internal.");
            exit;
        }

        if (isset($_POST["loginToken"]) && trim($_POST["loginToken"]) != "")     $loginToken = trim($_POST["loginToken"]);
        if (!isset($loginToken)) {
            echo composeReply("ERROR", "Akses tidak dikenal", array("API_ACTION" => "LOGOUT"));
            exit;
        }

        $stmt = $gPDO->prepare("SELECT * FROM _users WHERE U_LOGIN_TOKEN = ?");
        $stmt->execute([$loginToken]);
        $loginData = $stmt->fetch(PDO::FETCH_OBJ);
        if (!$loginData) {
            echo composeReply("ERROR", "User tidak dikenal", array("API_ACTION" => "LOGOUT"));
            exit;
        }
        $cekAdmin = $loginData->{"U_GROUP_ROLE"};
        if ($cekAdmin == 'customer') {
            echo composeReply("ERROR", "Anda tidak memiliki akses", array("API_ACTION" => "LOGOUT"));
            exit;
        }

        $i_id = "0";
        if (isset($_POST["i_id"]) && trim($_POST["i_id"]) != "") $i_id = trim($_POST["i_id"]);

        if ($i_id != "0") {
            $stmt = $gPDO->prepare("SELECT * FROM rs_item WHERE i_id = ?");
            $stmt->execute([$i_id]);
            $customerData = $stmt->fetch(PDO::FETCH_OBJ);
            if (!$customerData) {
                echo composeReply("ERROR", "Data item tidak dikenal");
                exit;
            }
        }

        if (isset($_POST["i_kategori"]) && trim($_POST["i_kategori"]) != "") $i_kategori = trim($_POST["i_kategori"]);
        if (!isset($i_kategori)) {
            echo composeReply("ERROR", "Harap isikan kategori item");
            exit;
        }

        if (isset($_POST["i_merk"]) && trim($_POST["i_merk"]) != "") $i_merk = trim($_POST["i_merk"]);
        if (!isset($i_merk)) {
            echo composeReply("ERROR", "Harap isikan merk item");
            exit;
        }

        if (isset($_POST["i_seri"]) && trim($_POST["i_seri"]) != "") $i_seri = trim($_POST["i_seri"]);
        if (!isset($i_seri)) {
            echo composeReply("ERROR", "Harap isikan seri item");
            exit;
        }

        if (isset($_POST["i_harga"]) && trim($_POST["i_harga"]) != "") $i_harga = trim($_POST["i_harga"]);
        if (!isset($i_harga)) {
            echo composeReply("ERROR", "Harap isikan harga item");
            exit;
        }

        // if (isset($_POST["i_jumlah"]) && trim($_POST["i_jumlah"]) != "") $i_jumlah = trim($_POST["i_jumlah"]);
        // if (!isset($i_jumlah)) {
        //     echo composeReply("ERROR", "Harap isikan jumlah item");
        //     exit;
        // }

        // if(isset($_POST["i_foto"]) && trim($_POST["i_foto"]) != "") $i_foto = trim($_POST["i_foto"]);
        // if(!isset($i_foto)) {
        //     echo composeReply("ERROR", "Harap isikan foto item");
        //     exit;
        // }

        //kosongan, buat Variabel !!!
        // if (isset($_POST["i_ketersediaan"]) && trim($_POST["i_ketersediaan"]) != "") $i_ketersediaan = trim($_POST["i_ketersediaan"]);


        if (isset($_POST["i_deskripsi"]) && trim($_POST["i_deskripsi"]) != "") $i_deskripsi = trim($_POST["i_deskripsi"]);
        if (!isset($i_deskripsi)) {
            echo composeReply("ERROR", "Harap isikan deskripsi item");
            exit;
        }

        //kosongan, buat Variabel !!!
        // if (isset($_POST["i_terpakai"]) && trim($_POST["i_terpakai"]) != "") $i_terpakai = trim($_POST["i_terpakai"]);


        if ($i_id == "0") { //create new
            // if (!isset($i_terpakai)) {
            //     $i_terpakai = "0";
            // }

            // if (!isset($i_ketersediaan)) {
            //     $i_ketersediaan = $i_jumlah;
            // }

            if (isset($_FILES["uploadFile"])) {
                $fileName = $_FILES['uploadFile']['name'];
                $fileSize = $_FILES['uploadFile']['size'];
                $fileTmp = $_FILES['uploadFile']['tmp_name'];
                $fileType = $_FILES['uploadFile']['type'];
                $fileError = $_FILES['uploadFile']['error'];

                $a = explode(".", $_FILES["uploadFile"]["name"]);
                $fileExt = strtolower(end($a));

                if (isset($fileError) && $fileError > 0) {
                    $FILE_UPLOAD_ERROR_INFO = array(
                        0 => 'There is no error, the file uploaded with success',
                        1 => 'Ukuran file terlalu besar'/*'The uploaded file exceeds the upload_max_filesize directive in php.ini'*/,
                        2 => 'Ukuran file terlalu besar'/*'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form'*/,
                        3 => 'Terjadi gangguan jaringan sehingga data terpotong'/*'The uploaded file was only partially uploaded'*/,
                        4 => 'Tidak ada file yang diunggah',
                        6 => 'Temporary folder tidak tersedia',
                        7 => 'Gagal menyimpan data',
                        8 => /*'A PHP extension stopped the file upload.'*/ 'Terjadi kesalahan internal di server',
                    );

                    echo composeReply("ERROR", $FILE_UPLOAD_ERROR_INFO[$fileError]);
                    exit;
                }

                $arrFileExt = array("jpg", "jpeg", "png");
                if (isset($fileName) && trim($fileName) != "") {
                    if (in_array($fileExt, $arrFileExt) === false) {
                        echo composeReply("ERROR", "Harap pilih format image yang sesuai");
                        exit;
                    }

                    $uploadFile = substr(md5(date("YmdHis")), 0, 5) . "." . $fileExt;
                    if (move_uploaded_file($fileTmp, "../uploads/" . $uploadFile)) {
                        $gPDO->prepare("INSERT INTO rs_item (i_kategori, i_merk, i_seri, i_harga, i_foto, i_deskripsi, create_at, update_at) VALUES (?,?,?,?,?,?,?,?)")->execute([$i_kategori, $i_merk, $i_seri, $i_harga, $uploadFile, $i_deskripsi, $loginData->{"U_ID"}, "-"]);
                        $i_id = $gPDO->lastInsertId();
                        if (isset($i_id) && $i_id > 0) {
                            echo composeReply("SUCCESS", "Item telah disimpan");
                            exit;
                        } else {
                            echo composeReply("ERROR", "Gagal menyimpan item");
                            exit;
                        }
                    } else {
                        echo composeReply("ERROR", "Gagal menyimpan item");
                        exit;
                    }
                } else {
                    echo composeReply("ERROR", "Gagal menyimpan item");
                    exit;
                }
            }



            $gPDO->prepare("INSERT INTO rs_item (i_kategori, i_merk, i_seri, i_harga, i_deskripsi, i_foto, create_at, update_at) VALUES (?,?,?,?,?,?,?,?)")->execute([$i_kategori, $i_merk, $i_seri, $i_harga, $i_deskripsi, '-', $loginData->{"U_ID"}, "-"]);
            $i_id = $gPDO->lastInsertId();
            if (isset($i_id) && $i_id > 0) {
                echo composeReply("SUCCESS", "Item telah disimpan");
                exit;
            } else {
                echo composeReply("ERROR", "Gagal menyimpan item");
                exit;
            }
        } else { //update
            //Pakai pada saat Customer ada yang pesan
            // $i_ketersediaan = $i_jumlah - $i_terpakai;

            //Ketika Admin Selesai
            // $stmt = $gPDO->prepare("SELECT i_ketersediaan FROM rs_item WHERE i_id = ?");
            // $stmt->execute([$i_id]);
            // $i_ketersediaan = $stmt->fetch(PDO::FETCH_OBJ);
            // $tampungan = $i_ketersediaan + $i_terpakai;
            // $i_ketersediaan = $tampungan;   

            if (isset($_FILES["uploadFile"])) {
                $fileName = $_FILES['uploadFile']['name'];
                $fileSize = $_FILES['uploadFile']['size'];
                $fileTmp = $_FILES['uploadFile']['tmp_name'];
                $fileType = $_FILES['uploadFile']['type'];
                $fileError = $_FILES['uploadFile']['error'];

                $a = explode(".", $_FILES["uploadFile"]["name"]);
                $fileExt = strtolower(end($a));

                if (isset($fileError) && $fileError > 0) {
                    $FILE_UPLOAD_ERROR_INFO = array(
                        0 => 'There is no error, the file uploaded with success',
                        1 => 'Ukuran file terlalu besar'/*'The uploaded file exceeds the upload_max_filesize directive in php.ini'*/,
                        2 => 'Ukuran file terlalu besar'/*'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form'*/,
                        3 => 'Terjadi gangguan jaringan sehingga data terpotong'/*'The uploaded file was only partially uploaded'*/,
                        4 => 'Tidak ada file yang diunggah',
                        6 => 'Temporary folder tidak tersedia',
                        7 => 'Gagal menyimpan data',
                        8 => /*'A PHP extension stopped the file upload.'*/ 'Terjadi kesalahan internal di server',
                    );

                    echo composeReply("ERROR", $FILE_UPLOAD_ERROR_INFO[$fileError]);
                    exit;
                }

                $arrFileExt = array("jpg", "jpeg", "png");
                if (isset($fileName) && trim($fileName) != "") {
                    if (in_array($fileExt, $arrFileExt) === false) {
                        echo composeReply("ERROR", "Harap pilih format image yang sesuai");
                        exit;
                    }

                    $uploadFile = substr(md5(date("YmdHis")), 0, 5) . "." . $fileExt;
                    if (move_uploaded_file($fileTmp, "../uploads/" . $uploadFile)) {
                        $gPDO->prepare("UPDATE rs_item SET i_kategori = ?, i_merk = ?, i_seri = ?, i_harga = ?, i_foto = ?, i_deskripsi = ?, update_at = ? WHERE i_id = ?")->execute([$i_kategori, $i_merk, $i_seri, $i_harga, $uploadFile, $i_deskripsi, $loginData->{"U_ID"}, $i_id]);
                        if (isset($i_id) && $i_id > 0) {
                            echo composeReply("SUCCESS", "Perubahan item telah disimpan");
                            exit;
                        } else {
                            echo composeReply("ERROR", "Gagal menyimpan perubahan item");
                            exit;
                        }
                    } else {
                        echo composeReply("ERROR", "Gagal menyimpan perubahan item");
                        exit;
                    }
                } else {
                    echo composeReply("ERROR", "Gagal menyimpan perubahan item");
                    exit;
                }
            }


            $gPDO->prepare("UPDATE rs_item SET i_kategori = ?, i_merk = ?, i_seri = ?, i_harga = ?, i_deskripsi = ?, update_at = ? WHERE i_id = ?")->execute([$i_kategori, $i_merk, $i_seri, $i_harga, $i_deskripsi, $loginData->{"U_ID"}, $i_id]);
            $gPDO->lastInsertId();
            if (isset($gPDO)) {
                echo composeReply("SUCCESS", "Perubahan item telah disimpan");
                exit;
            } else {
                echo composeReply("ERROR", "Gagal menyimpan perubahan item");
                exit;
            }
        }
    }
    if ($act == "delete_item") {
        if ($_SERVER["REQUEST_METHOD"] != "POST") {
            echo composeReply("ERROR", "[Routing ERROR] Internal error.");
            exit;
        }

        if (isset($_POST["loginToken"]) && trim($_POST["loginToken"]) != "")     $loginToken = trim($_POST["loginToken"]);
        if (!isset($loginToken)) {
            echo composeReply("ERROR", "Akses tidak dikenal", array("API_ACTION" => "LOGOUT"));
            exit;
        }

        $stmt = $gPDO->prepare("SELECT * FROM _users WHERE U_LOGIN_TOKEN = ?");
        $stmt->execute([$loginToken]);
        $userData = $stmt->fetch(PDO::FETCH_OBJ);
        if (!$userData) {
            echo composeReply("ERROR", "User tidak dikenal", array("API_ACTION" => "LOGOUT"));
            exit;
        }
        $cekAdmin = $userData->{"U_GROUP_ROLE"};
        if ($cekAdmin == 'customer') {
            echo composeReply("ERROR", "Anda tidak memiliki akses", array("API_ACTION" => "LOGOUT"));
            exit;
        }

        if (isset($_POST["i_id"]) && trim($_POST["i_id"]) != "") $i_id = trim(strtoupper($_POST["i_id"]));
        if (!isset($i_id)) {
            echo composeReply("ERROR", "Parameter tidak lengkap");
            exit;
        }

        $stmt = $gPDO->prepare("SELECT * FROM rs_item WHERE i_id = ?");
        $stmt->execute([$i_id]);
        $orderData = $stmt->fetch(PDO::FETCH_OBJ);
        if (!$orderData) {
            echo composeReply("ERROR", "Item tidak terdaftar");
            exit;
        }

        $gPDO->prepare("DELETE FROM rs_item WHERE i_id = ?")->execute([$i_id]);

        echo composeReply("SUCCESS", "Item berhasil di hapus");
        exit;
    }
} else {
    echo composeReply("ERROR", "[Routing ERROR] Terjadi kesalahan internal.");
    exit;
}
